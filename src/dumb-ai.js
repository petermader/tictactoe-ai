const util = require('./util.js');
const Game = require('./game.js');

module.exports = class DumbAI {
    constructor(symbol) {
        this.name = 'Dumb AI ' + util.uniqueId();
        this.symbol = symbol;
    }

    // will select the first free field
    async makeMove(game) {
        const { size } = game;
        for (let x = 0; x < size; x++) {
            for (let y = 0; y < size; y++) {
                if (game.board[x][y] === Game.NO_PLAYER) {
                    return [x, y];
                }
            }
        }
    }
}
