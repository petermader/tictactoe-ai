const BaseGame = require('./baseGame.js');
const { NO_PLAYER, PLAYER_A, PLAYER_B } = BaseGame;

module.exports = class SyncGame extends BaseGame {
  constructor(size, playerA, playerB){
    super(size, playerA, playerB);
  }

   makeMove(player) {
      const [x, y] = player.makeMove(this);
      if (!this.isOccupied(x, y)) {
          this.board[x][y] = player === this.playerA ? PLAYER_A : PLAYER_B;
          return true;
      }else {
        return false;
      }
  }

   run(print) {
      const { playerA, playerB } = this;

      for (;;) {
          //check if move is valid at all
          const properMovePlayerA = this.makeMove(playerA);
          this.winner = properMovePlayerA ? this.calculateWinner() : PLAYER_B;

          if (this.winner !== NO_PLAYER || this.isBoardFull()) {
              break;
          }

          if (print) {
            console.log(this.showBoard());
          }

          const properMovePlayerB = this.makeMove(playerB);
          this.winner = properMovePlayerB ? this.calculateWinner() : PLAYER_A;

          if (this.winner !== NO_PLAYER || this.isBoardFull()) {
              break;
          }

          if (print) {
            console.log(this.showBoard());
          }
      }

      return this.winner === PLAYER_A
          ? playerA
          : this.winner === PLAYER_B ? playerB : undefined;
  }
}
