const readline = require('readline');

// produces a range of numbers from 0 (inclusively) to 'n' (exclusively)
const range = module.exports.range = n => [...Array(n).keys()];

// returns a unique integer every time it is called
const uniqueId = module.exports.uniqueId = (_ => {
    let id = 0;
    return _ => id++;
})();

// flattens a 2d array
const flatten = module.exports.flatten = xs =>
    [].concat.apply([], xs);
 
// creates a rows-by-cols matrix from a 1d array
const shape = module.exports.shape = (arr, rows, cols) => {
    return range(rows).map((_, i) => arr.slice(i * cols, (i + 1) * cols));
};

// transposes a square matrix
const transpose = module.exports.transpose = xs =>
    xs[0].map((_, i) => xs.map(row => row[i]));

// writes 'question' to stdin and returns a promise
// containing the user's respose
const ask = question => new Promise((resolve, reject) => {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });


    rl.question(question, response => {
        rl.close();
        resolve(response);
    });
});

// continuously tries to read a number between 'min' and 'max'
// (both inclusively) from the console and returns a promise
// containing that number
const readIntBetween = module.exports.readIntBetween = async (min, max) => {
    for (;;) {
        const val = await ask(`(${min}-${max}): `);
        const num = parseInt(val, 10);
        if (num >= min && num <= max) {
            return num;
        }
    }
};

// first argument is a matrix and second is a vector that is rightmultiplied
// with the matrix. The result will be a vector once again.
// This method could be optimized most likely.
const matrixMultiplication = module.exports.matrixMultiplication = (matrix, vector) => {
    const vectorSize = vector.length;
    let resultVector = [];
    //the matrix is a 2D array, where each array defines a rows
    matrix.forEach((element, index) => {
        // first check if the 2D array is a proper matrix and the height of the matrix
        // is the same of the vector height
        if(element.length !== vectorSize){
            throw new Error("Bad Matrix!");
        }
        resultVector[index] = element.reduce((acc, value, index) => acc + value * vector[index], 0);
    });
    return resultVector;
}

// takes as input a vector of real numbers and returns a vector of numbers from 0 to 1
// R^n -> (0,1)^n
const logisticRegression = module.exports.logisticRegression = xs =>
    xs.map(x => 1 / (1 + Math.exp(-x)));
