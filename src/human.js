const util = require('./util.js');

module.exports = class Human {
    constructor(name, symbol) {
        this.name = name;
        this.symbol = symbol;
    }

    async makeMove(game) {
        console.log(game.showBoard());

        console.log(`${this.name}, make a move.`);

        console.log('x coordinate:');
        const x = await util.readIntBetween(1, game.size);
        console.log('y coordinate:');
        const y = await util.readIntBetween(1, game.size);

        if (game.isOccupied(x - 1, y - 1)) {
            console.log('Not a valid choice! Please try again!');
            return this.makeMove(game);
        }

        return [x - 1, y - 1];
    }
}
