const util = require('./util.js');

const getLineWinner = line =>
    new Set(line).size === 1 ? line[0] : NO_PLAYER;

const getLinesWinner = lines => lines.reduce(
    (winner, line) =>
        winner !== NO_PLAYER ? winner : getLineWinner(line),
    NO_PLAYER
);

const Game = module.exports = class Game {
    // Player A will start the game
    constructor(size, playerA, playerB) {
        this.playerA = playerA;
        this.playerB = playerB;
        this.size = size;

        this.board = util.range(size).map(
            _ => util.range(size).map(_ => NO_PLAYER)
        );

        this.gameOver = false;
        this.winner = NO_PLAYER;
    }

    getWinner() {
        return this.winner === PLAYER_A
            ? this.playerA
            : this.winner === PLAYER_B ? this.playerB : undefined;
    }

    isOccupied(x, y) {
        return this.board[x][y] !== NO_PLAYER;
    }


    showField(x, y) {
        const f = this.board[x][y];
        return f === NO_PLAYER
            ? ' '
            : f === PLAYER_A ? this.playerA.symbol : this.playerB.symbol;
    }

    showBoard() {
        const { board } = this;
        const rowDelimiter = '+' + '-'.repeat(this.size * 4 - 1) + '+\n';
        const rows = board.map(
            (_, y) => `| ${board.map((_, x) => this.showField(x, y)).join(' | ')} |`
        );

        return rowDelimiter + rows.join('\n' + rowDelimiter) + '\n' + rowDelimiter;
    }

    isBoardFull() {
        return this.gameOver = this.board.every(
            row => row.every(field => field !== NO_PLAYER)
        );
    }

    calculateWinner() {
        // TODO: the implementation of this method is long and ugly

        const { board } = this;

        // check columns
        const columnsWinner = getLinesWinner(board);
        if (columnsWinner !== NO_PLAYER) {
            return columnsWinner;
        }

        // check rows
        const rowsWinner = getLinesWinner(util.transpose(board));
        if (rowsWinner !== NO_PLAYER) {
            return rowsWinner;
        }

        // check diagonals
        const firstDiagonalWinner = getLineWinner(
            board.map((row, i) => row[i])
        );
        if (firstDiagonalWinner !== NO_PLAYER) {
            return firstDiagonalWinner;
        }

        const secondDiagonalWinner = getLineWinner(
            board.map((row, i) => row[this.size - i - 1])
        );
        if (secondDiagonalWinner !== NO_PLAYER) {
            return secondDiagonalWinner;
        }

        return NO_PLAYER;
    }
}

Game.NO_PLAYER = Symbol();
Game.PLAYER_A = Symbol('a');
Game.PLAYER_B = Symbol('b');

const { NO_PLAYER, PLAYER_A, PLAYER_B } = Game;
