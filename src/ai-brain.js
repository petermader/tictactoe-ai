const util = require('./util.js');
const { matrixMultiplication: matMul, logisticRegression } = util;

module.exports = class NeuralNetwork {
    // if you call this constructor, make sure the matrices you specify are of
    // the correct size!
    constructor(inputWeightMatrix, hiddenWeightMatrices, outputWeightMatrix) {
        this.inputWeightMatrix = inputWeightMatrix;
        this.hiddenWeightMatrices = hiddenWeightMatrices;
        this.outputWeightMatrix = outputWeightMatrix;
    }

    // run a forward pass through the network
    // the input should contain a array representing the state of
    // the board where
    // *  0 means that the field is empty,
    // *  1 means that the AI this brain is associated with occupies the field
    // * -1 means the opponent occupies the field
    getMove(boardArray) {
        const firstActivation = logisticRegression(
            matMul(this.inputWeightMatrix, boardArray)
        );

        // compute the activation for each matrix and feed it into the next
        // layer
        const activationBeforeOutput = this.hiddenWeightMatrices.reduce(
            (activation, hiddenWeightMatrix) =>
                logisticRegression(matMul(hiddenWeightMatrix, activation)),
            firstActivation
        );

        // compute the output
        return logisticRegression(
            matMul(this.outputWeightMatrix, activationBeforeOutput)
        );
    }

    getInputMatrix() {
        return this.inputWeightMatrix;
    }

    getHiddenMatrices() {
        return this.hiddenWeightMatrices;
    }

    getOutputMatrix() {
        return this.outputWeightMatrix;
    }
}
