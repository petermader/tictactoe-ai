const Generation = require('./generation.js');
const Chromosome = require('./nn-ai.js');
const SyncGame = require('./syncGame.js');

// test Generation
// create 10 chromosomes
const chroms = Array.from({length: 10}).map((_, index) => new Chromosome(index, undefined));
console.log('Generation 0: \n' + chroms.join('\n'));
const gen = new Generation(chroms, 0, 0).runGeneration();
console.log('\nGeneration 1: \n' + gen.join('\n'));

// run a game with logging enabled
console.log('\nSimulating a game between two surviving AIs:');
console.log(`${gen[0]} vs ${gen[1]}`);
const game = new SyncGame(3, gen[0], gen[1]);
game.run(true);

console.log(`Winner: ${game.getWinner().toString()}`)

// -----------------------
// TEST THE NEURAL NETWORK
// -----------------------

const util = require('./util.js');
const generateWeightMatrix = (rows, cols) => {
    return util.range(rows).map(
        _ => util.range(cols).map(_ => Math.floor(Math.random() * 8))
    );
}

const NeuralNetworkAI = require('./nn-ai.js');
const NeuralNetwork = require('./ai-brain.js');
const inputMatrix = generateWeightMatrix(3, 2);
const hiddenMatrices = [generateWeightMatrix(1, 3), generateWeightMatrix(3, 1)];
const outputMatrix = generateWeightMatrix(2, 3);

const brain = new NeuralNetwork(inputMatrix, hiddenMatrices, outputMatrix);
const chrom = new NeuralNetworkAI('@');
chrom.brain = brain;
const weights = chrom.getWeights();

console.log(brain);
console.log(weights.join(', '));
console.log(NeuralNetworkAI.createBrainFromWeights(weights, 2, [3, 1, 3], 2));
