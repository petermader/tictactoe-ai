const BaseGame = require('./baseGame');
const { NO_PLAYER, PLAYER_A, PLAYER_B } = BaseGame;


module.exports = class AsyncGame extends BaseGame {
  constructor(size, playerA, playerB){
    super(size, playerA, playerB);
  }

  async makeMove(player) {
      const [x, y] = await player.makeMove(this);
      if (!this.isOccupied(x, y)) {
          this.board[x][y] = player === this.playerA ? PLAYER_A : PLAYER_B;
          return true;
      } else {
          return false;
      }
  }

  async run() {
      const { playerA, playerB } = this;

      for (;;) {
          //check if move is valid at all
          const properMovePlayerA = await this.makeMove(playerA);
          this.winner = properMovePlayerA ? this.calculateWinner() : playerB;

          if (this.winner !== NO_PLAYER || this.isBoardFull()) {
              break;
          }

          const properMovePlayerB = await this.makeMove(playerB);
          this.winner = properMovePlayerB ? this.calculateWinner() : playerA;

          if (this.winner !== NO_PLAYER || this.isBoardFull()) {
              break;
          }
      }

      return this.winner === PLAYER_A
          ? playerA
          : this.winner === PLAYER_B ? playerB : undefined;
  }
}
