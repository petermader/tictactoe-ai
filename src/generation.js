const SyncGame = require('./syncGame');

module.exports = class Generation {
    constructor(chromosomes, crossoverRate, mutationRate) {
        //chromosomes is an array of AIs
        this.chromosomes = chromosomes;
        this.size = chromosomes.length;
        this.crossoverRate = crossoverRate;
        this.mutationRate = mutationRate;
    }

    runGeneration() {
        const numberOfWins = this.determineWins();
        const fitnessProbabilities = this.calculateFitness(numberOfWins);
        return this.selectNewGeneration(fitnessProbabilities);
    }

    determineWins() {
        const { chromosomes } = this;
        return chromosomes.map(chrom => chromosomes.reduce((acc, other) => {
            if (chrom === other) {
                return acc;
            }

            // let chrom and other fight each other
            const game = new SyncGame(3, chrom, other);

            // determine the winner
            const win = game.run() === chrom ? 1 : 0;

            return acc + win;
        }, 0));
    }

    //input: array with the number of wins of the specific chromosome
    //output: array with the probability range of all chromosomes
    calculateFitness(numberOfWins) {
        const total = numberOfWins.reduce((sum, val) => {
            return sum + val;
        });
        const fitness = numberOfWins.map(entry => {
            return entry / total;
        });
        let sum = 0;
        const fitnessProbabilities = fitness.map(entry => {
            sum += entry;
            return sum;
        });
        return fitnessProbabilities;
    }

    selectNewGeneration(fitnessProbabilities) {
        const { chromosomes } = this;
        const newGen = fitnessProbabilities.map(_ => {
            const randomNumber = Math.random();
            const index = fitnessProbabilities.findIndex(x => x > randomNumber);

            // at this point, the AI will have to be copied
            return chromosomes[index];
        });
        return newGen;
    }

    crossover(newGen) {
        //flatten out the genes into a 1D Array

    }

    mutation() {

    }
}
