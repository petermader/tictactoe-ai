const util = require('./util.js');
const NeuralNetwork = require('./ai-brain.js');
const BaseGame = require('./baseGame.js');

module.exports = class NeuralNetworkAI {
    constructor(symbol, weights) {
        this.name = 'Neural network AI ' + util.uniqueId();
        this.symbol = symbol;

        if (weights) {
            this.brain = NeuralNetworkAI.createBrainFromWeights(weights, 9, [10, 10, 10], 9);
            return;
        }

        // construct a NN with three hidden layers, each
        // containing 10 neurons, with weights from -1 to 1
        const initialWeightRange = .1;
        const inputMatrix = NeuralNetworkAI.generateWeightMatrix(10, 9, initialWeightRange);
        const hiddenMatrices = [
            NeuralNetworkAI.generateWeightMatrix(10, 10, initialWeightRange),
            NeuralNetworkAI.generateWeightMatrix(10, 10, initialWeightRange)
        ];
        const outputMatrix = NeuralNetworkAI.generateWeightMatrix(9, 10, initialWeightRange);

        this.brain = new NeuralNetwork(inputMatrix, hiddenMatrices, outputMatrix);
    }

    // generates a rows-by-cols matrix containing weights from -range to range
    static generateWeightMatrix(rows, cols, range) {
        return util.range(rows).map(
            _ => util.range(cols).map(_ => Math.random() * 2 * range - range)
        );
    }

    // turns a 1d array into a brain
    static createBrainFromWeights(weights, inputSize, hiddenSizes, outputSize) {
        const hasHiddenLayers = hiddenSizes.length > 0;
        const nextLayerSize = hasHiddenLayers ? hiddenSizes[0] : outputSize;
        let currentIndex = inputSize * nextLayerSize;

        // compute the input matrix
        const inputWeights = weights.slice(0, currentIndex);
        const inputMatrix  = util.shape(inputWeights, nextLayerSize, inputSize);
        
        const hiddenAndOutputMatrices = hiddenSizes.map((size, index) => {
            const nextLayerSize = index >= hiddenSizes.length - 1 ? outputSize : hiddenSizes[index + 1];
            const newIndex = currentIndex + size * nextLayerSize;

            const hiddenWeights = weights.slice(currentIndex, newIndex);
            const hiddenLayer   = util.shape(hiddenWeights, nextLayerSize, size);
            currentIndex = newIndex;
            return hiddenLayer;
        });

        const hiddenMatrices = hiddenAndOutputMatrices.slice(0, -1);
        const outputMatrix   = hiddenAndOutputMatrices[hiddenAndOutputMatrices.length - 1];

        return new NeuralNetwork(inputMatrix, hiddenMatrices, outputMatrix);
    }

    // turns the brain's weights into a 1d array
    getWeights() {
        const { brain } = this;
        const allMatrices = brain.getInputMatrix().concat(
            ...brain.getHiddenMatrices(), brain.getOutputMatrix()
        );
        return util.flatten(allMatrices);
    }

    makeMove(game) {
        const opponent = game.playerA === this ? BaseGame.PLAYER_B : BaseGame.PLAYER_A;
        const boardArray = util.flatten(game.board).map(
            x => x === BaseGame.NO_PLAYER ? 0 : x === opponent ? -1 : 1
        );

        const output = this.brain.getMove(boardArray);
        const index = output.indexOf(Math.max(...output));
        const x = Math.floor(index / game.size);
        const y = index % game.size;

        // if this move is invalid, the game object will cause
        // this player to lose
        return [x, y];
    }

    toString() {
        return `${this.name} (Symbol: ${this.symbol})`;
    }
}
